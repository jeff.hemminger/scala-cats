import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Arbitrary
import cats.kernel.Eq
import algebras.catswork.Person

package object algebras {
  implicit val personEq: Eq[Person] = Eq.instance[Person](_.toString == _.toString)

  implicit val arbPer = Arbitrary{
    for {
      fn <- arbitrary[String]
      ln <- arbitrary[String]
    } yield Person(fn, ln)
  }
}
