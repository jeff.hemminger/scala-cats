package algebras

import org.scalatest._
import algebras.catswork._
import org.typelevel.discipline.scalatest.Discipline
import cats.kernel.laws.discipline.{ SemigroupTests, MonoidTests }

class AlgebrasSpec extends FunSuite with Discipline {

  checkAll("PersonSemigroup", SemigroupTests(personSemigroup).semigroup)
  checkAll("PersonMonoid", MonoidTests(personMonoid).monoid)
}
